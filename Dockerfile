FROM centos:7

ENV APP_PATH=/root/labRemote
WORKDIR $APP_PATH
COPY . .
ARG USE_PYTHON=on
RUN yum -y install epel-release && \
    yum -y update && \
    yum -y install make gcc gcc-c++ cmake3 \
                   autoconf automake git swig \
                   json-devel \
                   python3-devel \
                   libftdi-devel libftdi-c++-devel \
                   opencv-devel \
                   libusb-devel && \
    yum clean all && \
    rm -rf /var/cache/yum && \
    git clone https://github.com/l29ah/libmpsse.git && \
    cd libmpsse/src && \
    git checkout -b v1.3.2  v1.3.2 && \
    autoreconf && \
    ./configure CFLAGS="-std=c99" --disable-python && \
    make && make install && \
    cd ../../ && \
    mkdir build && cd build && \
    cmake3 -DUSE_PYTHON=$USE_PYTHON .. && \
    make

ENV PATH=$APP_PATH/build/bin:$PATH
ENV PYTHONPATH=$PWD/lib:$PYTHONPATH
