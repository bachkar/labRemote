add_library(Zaber SHARED)
target_sources(Zaber
  PRIVATE
  za_serial.cpp
)
target_include_directories(Zaber PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
