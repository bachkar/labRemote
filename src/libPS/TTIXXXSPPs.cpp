#include "TTIXXXSPPs.h"

#include <algorithm>
#include <thread>

#include "Logger.h"

//Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(TTIXXXSPPs)

TTIXXXSPPs::TTIXXXSPPs(const std::string& name) :
TTIPs(name, {"TSX1820P"}, 1)
{ }
