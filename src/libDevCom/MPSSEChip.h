#ifndef MPSSE_CHIP_H
#define MPSSE_CHIP_H

//std/stl
#include <string>
#include <vector>

//libmpsse
struct mpsse_context;

//! \brief Base implementation of a device with a Multi-Protocol Synchronous Serial Engine (MPSSE).
/**
 * The `MPSSEChip` class provides the interface to the MPSSE device and is the main
 * interface to the C library `libmpsse`. Devices in `libDevCom` that communicate
 * via MPSSE should inherit from `MPSSEChip`, and use it as the primary means of
 * interfacing with the device. In this way, the `libmpsse` methods are abstracted away.
 */
class MPSSEChip
{
    public:
        //! \brief Possible MPSSE serial communication protocols supported by `libmpsse`.
        /**
        * It should be noted that for the SPI protocols, `libmpsse` appears to set
        * the chip-select (CS) pin to 0x08 (ADBUS3). In actuality, any of the GPIOs
        * should be able to be set as a CS line for an SPI secondary. This means
        * that `libmpsse` assumes one SPI primary and one SPI secondary.
        */
        enum class Protocol {
            I2C
            ,SPI0 // note: libmpsse only supports SPI bus with single primary and single secondary (i.e. only a single CS line)
            ,SPI1
            ,SPI2
            ,SPI3
        };

        //! \brief Serial communication clock speeds supported by the various serial communication protocol.
        enum class Speed {
            ONE_HUNDRED_KHZ   = 100000
            ,FOUR_HUNDRED_KHZ = 400000
            ,ONE_MHZ          = 1000000
            ,TWO_MHZ          = 2000000
            ,FIVE_MHZ         = 5000000
            ,SIX_MHZ          = 6000000
            ,TEN_MHZ          = 10000000
            ,TWELVE_MHZ       = 12000000
            ,FIFTEEN_MHZ      = 15000000
            ,THIRTY_MHZ       = 30000000
            ,SIXTY_MHZ        = 60000000
        };

        //! \brief Expected endianness of data transfer between the MPSSE serial communication primary and secondary devices on the serial bus
        enum class Endianness {
            MSBFirst
            ,LSBFirst
        };

        //! \brief `MPSSEChip` constructor, instantiates an `mpsse_context` configured for serial communication.
        /**
        * \param protocol Serial communication protocol required (see `Protocol`).
        * \param speed Clock rate for the synchronous communication (see `Speed`).
        * \param endianness Whether the devices expects MSB or LSB first in data transmission (see `Endianness`).
        * \param vendor_id USB device vendor ID of the device to open communication with.
        * \param product_id USB device product ID of the device to open communication with.
	* \param description Product description used to identify device to open communication with (leave blank for none).
	* \param serial Serial number used to identify device to open communication with (leave blank for none).
        *
        * The default behavior, with `vendor_id` and `product_id` both equal to `-1` tells the constructor
        * to attempt communication with the first FTDI USB device found. If both `vendor_id` and
        * `product_id` are non-negative, then the `MPSSEChip` constructor will attempt to
        * establish communication with the FTDI USB device with the given `vendor_id` and `product_id`.
	*
	* The product description and serial number can be used in a manner similar to the vendor and product ID's, with empty
	* string disabling their check.
	*
        * Running `lsusb` at the command line lists the possible USB devices on your machine (along with their
        * vendor and product IDs).
        */
        MPSSEChip(Protocol protocol, Speed speed, Endianness endianness,
                    int vendor_id = -1, int product_id = -1, const std::string& description = "", const std::string& serial = "");

        //! \brief `MPSSEChip` destructor, closes the `mpsse_context` and USB device.
        ~MPSSEChip();

        //! \brief Establish the `START` condition on the serial communication bus.
        virtual bool start();

        //! \brief Establish the `STOP` condition on the serial communication bus.
        virtual bool stop();

        //! \brief Write data to the serial bus.
        /**
        * \param data Array of data (bytes) to send.
        * \param size Number of bytes to write.
        */
        virtual bool write(char* data, int size);

        //! \brief Read data from the serial bus.
        /**
        * \param size Number of bytes to read.
        */
        virtual char* read(int size);

        //! \brief Set the state of one of the GPIO pins on the MPSSE chip.
        /**
        * \param pin The GPIO pin to set.
        * \param state The state to set the pin to (`0` is OFF/LOW, `1` is ON/HIGH)
        *
        * Note that in terms of the MPSSE device, the GPIO pin numbering starts
        * counting from `0`, and does not correspond to the actual/physical pin number of
        * the MPSSE chip. For I2C communication, GPIO pin `0` corresponds to A*BUS3.
        * For SPI communication, GPIO pin `0` corresponds to (A*BUS4 + (N SPI secondaries) - 1).
        */
        virtual void gpio_write(int pin, int state);

        //! \brief Get the ACK bit from a the most recent I2C write to an I2C secondary.
        virtual bool get_ack();

        //! \brief Set the I2C primary's (i.e. that of the `MPSSEChip`) ACK bit for subsequent I2C transactions.
        virtual void set_ack();

        //! \brief Set the I2C primary's (i.e. that of the `MPSSEChip`) NACK bit for subsequent I2C transactions.
        virtual void set_nack();

        //! \brief Get the currently configured `MPSSE` serial communication protocol.
        const Protocol& protocol() const { return m_protocol; }

        //! \brief Get the bus speed for the currently configured serial communication.
        const Speed& speed() const { return m_speed; }

        //! \brief Get the expected I2C device endianness.
        const Endianness& endianness() const { return m_endianness; }

        //! \brief Return a string summarizing the currently configured `MPSSEChip` instance.
        std::string to_string();

        //! \breif Convert an `MPSSEChip::Protocol` value to a human-readable `std::string`.
        static std::string protocol2str(const Protocol& proto);

        //! \breif Convert an `MPSSEChip::Speed` value to a human-readable `std::string`.
        static std::string speed2str(const Speed& speed);

        //! \brief Convert an `MPSSEChip::Endianness` value to a human-readable `std::string`.
        static std::string endianness2str(const Endianness& endianness);

    private:

        bool is_known_ftdi_device(int vendor_id, int product_id);
        std::vector<std::pair<int,int> > m_known_ftdi_devices = {
            std::make_pair(0x0403, 0x6010),
            std::make_pair(0x0403, 0x6011),
            std::make_pair(0x0403, 0x6014)
        };

        struct mpsse_context* m_mpsse;
        struct mpsse_context* mpsse_ctx() { return m_mpsse; }

        Protocol m_protocol;
        Speed m_speed;
        Endianness m_endianness;

}; // class MPSSEChip

#endif // MPSSE_CHIP_H
